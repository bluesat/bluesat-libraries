# Instructions for use
## General instructions
1. First update the repository by using the command **git pull**
2. Make any of the changes from the list below
3. Add the files whose changes you want to track with **git add <name of file>**
4. Commit the version of changes you want for those files using **git commit -m
   "<Insert a meaningful comment regarding the change you made>"**
5. Upload the changes using the command **git push**

### Creating a new library
1. Go to KiCAD -> library editor -> File -> New Library
2. Name the file bluesat-<category> and press OK
### Importing an existing library
1. Go to KiCAD -> library editor -> Preferences -> Manage Symbol Libraries
2. Click Browse Libraries and highlight all of the libraries in this repository
### Adding to an existing library
1. Go to KiCAD -> library editor -> Create new symbol
2. Select the library to add to, and fill in all the fields
3. Remember to save the library before you push your changes
