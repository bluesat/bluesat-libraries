# Instructions for use
## General instructions
1. First update the repository by using the command **git pull**
2. Make any of the changes from the list below
3. Add the files whose changes you want to track with **git add <name of file>**
4. Commit the version of changes you want for those files using **git commit -m
   "<Insert a meaningful comment regarding the change you made>"**
5. Upload the changes using the command **git push**

### Creating a new library
1. Go to KiCAD -> footprint library editor.
    You must create a new footprint to place in the library.
2. Click New Footprint and enter a name for the footprint (not the library).
3. You may now create a new library and save the footprint in it:
4. Select "Create new library and save current footprint".
5. Browse to the desired path base, which should end in bluesat-footprints.
6. Under the 'library folder' section, move cursor to end of path and add:
        \bluesat-<component_category>
7. Choose the category name based on the components in the library. For example, a library of 
    inductors would be: \bluesat-inductors
8. Click OK.
9. Don't forget to add this new library in the library manager under Preferences -> Manage footprint libraries
10. You can now create your footprint and it will save to this library. You may have to set the new library as
    the active library with the 'select active library' button.
### Adding to an existing library
1. Go to KiCAD -> footprint library editor -> new footprint
2. Select the active library
3. Remember to save the library before you push your changes
